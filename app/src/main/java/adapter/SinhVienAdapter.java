package adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.quanlisinhviensqlite.ListSinhVienActivity;
import com.example.quanlisinhviensqlite.R;
import com.example.quanlisinhviensqlite.SinhVienDetailActivity;

import java.util.List;

import model.Lop;
import model.SinhVien;

public class SinhVienAdapter extends BaseAdapter {
    private Context context;
    private int layout;
    private List<SinhVien> sinhVienList;

    public SinhVienAdapter(Context context, int layout, List<SinhVien> sinhVienList) {
        this.context = context;
        this.layout = layout;
        this.sinhVienList = sinhVienList;
    }

    @Override
    public int getCount() {
        return sinhVienList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private class ViewHolder{
        TextView tenSinhVien;
        TextView lop;
        TextView masv;
        LinearLayout itemSV;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = new ViewHolder();

        if (convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_sinhvien, null);

            holder.tenSinhVien = convertView.findViewById(R.id.tenSinhVien);
            holder.lop  = convertView.findViewById(R.id.tvlop);
            holder.masv  = convertView.findViewById(R.id.maSinhVien);


            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }
        final SinhVien sinhVien = sinhVienList.get(position);
        holder.tenSinhVien.setText(sinhVien.getTen());
        holder.lop.setText(sinhVien.getLop());
        holder.masv.setText(sinhVien.getMsv());

        holder.itemSV = convertView.findViewById(R.id.item_Sv);
        holder.itemSV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickGoToDeTailSinhVien(sinhVien);
            }
        });
        return convertView;
        }
    public void  onClickGoToDeTailSinhVien(SinhVien sv){
        Intent intent = new Intent(context, SinhVienDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("objectSV",  sv);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }
    }

