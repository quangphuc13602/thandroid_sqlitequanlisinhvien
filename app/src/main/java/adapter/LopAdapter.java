package adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.quanlisinhviensqlite.ListSinhVienActivity;
import com.example.quanlisinhviensqlite.R;

import java.io.Serializable;
import java.util.List;

import model.Lop;

public class LopAdapter extends BaseAdapter {
    private Context context;
    private int layout;
    private List<Lop> lopList;

    public LopAdapter(Context context, int layout, List<Lop> lopList) {
        this.context = context;
        this.layout = layout;
        this.lopList = lopList;
    }

    @Override
    public int getCount() {
        return lopList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public class LopViewHolder{
        TextView tenLop;
        LinearLayout itemLop;

    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LopViewHolder holder;
        if (convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_lop, null);

            holder = new LopViewHolder();
            holder.tenLop = convertView.findViewById(R.id.tenLop);
            convertView.setTag(holder);
        }else {
            holder = (LopViewHolder) convertView.getTag();
        }
        final Lop lop = lopList.get(position);
        holder.tenLop.setText(lop.getTenLop());

        holder.itemLop = convertView.findViewById(R.id.itemlop);
        holder.itemLop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickGoToSinhVienList(lop);
            }
        });

        return convertView;
    }
    public void  onClickGoToSinhVienList(Lop tenlop){
        Intent intent = new Intent(context, ListSinhVienActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("objectLop",  tenlop);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

}
