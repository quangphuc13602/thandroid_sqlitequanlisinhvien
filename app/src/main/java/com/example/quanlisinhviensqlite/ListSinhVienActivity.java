package com.example.quanlisinhviensqlite;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

import adapter.SinhVienAdapter;
import helper.LopHelper;
import helper.SinhVienHelper;
import model.Lop;
import model.SinhVien;

public class ListSinhVienActivity extends AppCompatActivity {

    SinhVienHelper sinhVienHelper;
    SinhVienAdapter sinhVienAdapter;
    ArrayList<SinhVien> listSV;
    ListView lvSinhVien;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_sinhvien);


        //create item SinhVien
        lvSinhVien = findViewById(R.id.listSinhVien);
        listSV = new ArrayList<>();
        sinhVienAdapter = new SinhVienAdapter(this, R.layout.item_sinhvien, listSV);
        lvSinhVien.setAdapter(sinhVienAdapter);
        //create database
        sinhVienHelper = new SinhVienHelper(this, "quanlisinhvien.sqlite",null,1);
        //create table
        sinhVienHelper.QueryData("CREATE TABLE IF NOT EXISTS SINHVIEN(ID VARCHAR(50) PRIMARY KEY , Ten VARCHAR(50), LOP VARCHAR(10))");
        //insert into
//        sinhVienHelper.QueryData("INSERT INTO SINHVIEN VALUES('2050', 'Phuc', '20T2')");
//        sinhVienHelper.QueryData("INSERT INTO SINHVIEN VALUES('2030', 'Luc', '20T2')");
//        sinhVienHelper.QueryData("INSERT INTO SINHVIEN VALUES('2010', 'Duy', '20T1')");
//        sinhVienHelper.QueryData("INSERT INTO SINHVIEN VALUES('2020', 'QUoc', '20T1')");
//        sinhVienHelper.QueryData("INSERT INTO SINHVIEN VALUES('2040', 'Ngan', '20T3')");
//        sinhVienHelper.QueryData("INSERT INTO SINHVIEN VALUES('2060', 'Manh', '20T3')");
        //create table LOP
        sinhVienHelper.QueryData("CREATE TABLE IF NOT EXISTS LOP(ID INTEGER PRIMARY KEY AUTOINCREMENT, TENLOP VARCHAR(10))");
        //INSERT INTO
//        lopHelper.QueryData("INSERT INTO LOP VALUES (null, '20T1')");
//        lopHelper.QueryData("INSERT INTO LOP VALUES (null, '20T2')");
//        lopHelper.QueryData("INSERT INTO LOP VALUES (null, '20T3')");



        //show data
        Bundle bundle = getIntent().getExtras();
        if(bundle == null){
            return;
        }
        Lop lop = (Lop) bundle.get("objectLop");
        String tenlop = lop.getTenLop();

        Cursor dataSinhVien = sinhVienHelper.GetData("select * from SINHVIEN where LOP = '" + tenlop + "'");
        while (dataSinhVien.moveToNext()) {
            String id = dataSinhVien.getString(0);
            String ten = dataSinhVien.getString(1);
            String lopsv = dataSinhVien.getString(2);
            listSV.add(new SinhVien(ten, id, lopsv));
        }
        sinhVienAdapter.notifyDataSetChanged();

        //add Sinh vien
        Button btnAdd = findViewById(R.id.btnAdd);
        EditText tenSV = (EditText) findViewById(R.id.etNameStudent);
        String tvTEN =  tenSV.getText().toString();
        EditText masv = (EditText) findViewById(R.id.etIDStudent);
        String tvMASV =  tenSV.getText().toString();
        EditText lopsv = (EditText) findViewById(R.id.etclasses);
        lopsv.setText(lop.getTenLop());
        String tvLOP =  lop.getTenLop();

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sinhVienHelper.QueryData("INSERT INTO SINHVIEN VALUES('"+ masv.getText().toString() +"', '"+tenSV.getText().toString()+"', '"+tvLOP+"')");
                Toast.makeText(ListSinhVienActivity.this, "Thêm thành công", Toast.LENGTH_SHORT).show();
                while (dataSinhVien.moveToNext()) {
                    String id = dataSinhVien.getString(0);
                    String ten = dataSinhVien.getString(1);
                    String lopsv = dataSinhVien.getString(2);
                    listSV.add(new SinhVien(ten, id, lopsv));
                }
                sinhVienAdapter.notifyDataSetChanged();
            }
        });
    Button btnDelete = findViewById(R.id.btnDelete);
    btnDelete.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            tenSV.setText("");
            masv.setText("");
        }
    });
    }
}