package com.example.quanlisinhviensqlite;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;

import helper.SinhVienHelper;
import model.SinhVien;

public class SinhVienDetailActivity extends AppCompatActivity {
    SinhVienHelper sinhVienHelper;
    ArrayList<SinhVien> listSV = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sinhvien_detail);
        sinhVienHelper = new SinhVienHelper(this, "quanlisinhvien.sqlite",null,1);

        sinhVienHelper.QueryData("CREATE TABLE IF NOT EXISTS DIEM(ID VARCHAR(10) PRIMARY KEY, TOAN VARCHAR(5),LY VARCHAR(5),HOA VARCHAR(5))");
//        sinhVienHelper.QueryData("INSERT INTO DIEM VALUES('2010', '9', '9','9')");
//        sinhVienHelper.QueryData("INSERT INTO DIEM VALUES('2020', '9', '9','9')");
//        sinhVienHelper.QueryData("INSERT INTO DIEM VALUES('2030', '9', '9','9')");
//        sinhVienHelper.QueryData("INSERT INTO DIEM VALUES('2032', '9', '9','9')");
//        sinhVienHelper.QueryData("INSERT INTO DIEM VALUES('2040', '9', '9','9')");
//        sinhVienHelper.QueryData("INSERT INTO DIEM VALUES('2050', '9', '9','9')");
//        sinhVienHelper.QueryData("INSERT INTO DIEM VALUES('2060', '9', '9','9')");
//        sinhVienHelper.QueryData("INSERT INTO DIEM VALUES('2070', '9', '9','9')");
//        sinhVienHelper.QueryData("INSERT INTO DIEM VALUES('2080', '9', '9','9')");
//        sinhVienHelper.QueryData("INSERT INTO DIEM VALUES('2090', '9', '9','9')");

        TextView tvten = findViewById(R.id.tvtensvdetail);
        TextView tvmasv = findViewById(R.id.tvmasvdetail);
        TextView tvlop = findViewById(R.id.tvlopdetail);
        TextView tvtoan = findViewById(R.id.tvdiemtoan);
        TextView tvli = findViewById(R.id.tvdiemli);
        TextView tvhoa = findViewById(R.id.tvdiemhoa);
        TextView tvdiemtb = findViewById(R.id.tvdiemTB);


        Bundle bundle = getIntent().getExtras();
        if(bundle == null){
            return;
        }
        SinhVien sinhVien = (SinhVien) bundle.get("objectSV");


        Double toan, li, hoa;
        String masv = sinhVien.getMsv();
        tvmasv.setText(masv);
        String tensv = sinhVien.getTen();
        tvten.setText(tensv);
        String lop = sinhVien.getLop();
        tvlop.setText(lop);
        Cursor dataDiem = sinhVienHelper.GetData("select * from DIEM where ID = '" + masv + "'");
        while (dataDiem.moveToNext()) {
            String id = dataDiem.getString(0);
            String diemToan = dataDiem.getString(1);
            tvtoan.setText("Toán Học : "+diemToan);
            toan = Double.parseDouble(diemToan);
            String diemLy = dataDiem.getString(2);
            tvli.setText("Vật Lý : "+diemLy);
            li = Double.parseDouble(diemLy);

            String diemHoa = dataDiem.getString(3);
            tvhoa.setText("Hóa Học : "+diemHoa);
            hoa = Double.parseDouble(diemHoa);

            Double tb = (toan + li + hoa) / 3;
            tvdiemtb.setText(tb.toString());
            listSV.add(new SinhVien(id , diemToan, diemLy, diemHoa));
        }
        Button btnDelete = findViewById(R.id.btnDeleteSVdetail);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sinhVienHelper.QueryData("delete from DIEM where ID = '"+masv+"'");
                sinhVienHelper.QueryData("delete from SINHVIEN where ID = '"+masv+"'");
                Intent intent  = new Intent(SinhVienDetailActivity.this, ListLopActivity.class);
                startActivity(intent);
            }
        });
    }
}