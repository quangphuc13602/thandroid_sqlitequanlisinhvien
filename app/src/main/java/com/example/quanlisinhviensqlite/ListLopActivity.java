package com.example.quanlisinhviensqlite;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import adapter.LopAdapter;
import helper.LopHelper;
import helper.SinhVienHelper;
import model.Lop;

public class ListLopActivity extends AppCompatActivity {

    LopHelper lopHelper;
    LopAdapter lopAdapter;
    ArrayList<Lop> listLop;
    ListView lvLop;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_lop);

        //create Lop
        lvLop = findViewById(R.id.listviewLop);
        listLop = new ArrayList<>();
        lopAdapter = new LopAdapter(this, R.layout.item_lop, listLop);
        lvLop.setAdapter(lopAdapter);
        //create database
        lopHelper = new LopHelper(this, "quanlisinhvien.sqlite",null,1);
        //create table LOP
        lopHelper.QueryData("CREATE TABLE IF NOT EXISTS LOP(ID INTEGER PRIMARY KEY AUTOINCREMENT, TENLOP VARCHAR(10))");
        //INSERT INTO
//        lopHelper.QueryData("INSERT INTO LOP VALUES (null, '20T1')");
//        lopHelper.QueryData("INSERT INTO LOP VALUES (null, '20T2')");
//        lopHelper.QueryData("INSERT INTO LOP VALUES (null, '20T3')");
        //showdata
        Cursor dataLop = lopHelper.GetData("select * from LOP");
        while (dataLop.moveToNext()){
            String id = dataLop.getString(0);
            String lop = dataLop.getString(1);
            listLop.add(new Lop(id, lop));
        }
        lopAdapter.notifyDataSetChanged();



    }
}