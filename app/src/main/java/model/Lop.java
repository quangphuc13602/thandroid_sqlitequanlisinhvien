package model;

import java.io.Serializable;

public class Lop implements Serializable {
    String idLop;
    String tenLop;

    public Lop(String idLop, String tenLop) {
        this.idLop = idLop;
        this.tenLop = tenLop;
    }

    public String getIdLop() {
        return idLop;
    }

    public void setIdLop(String idLop) {
        this.idLop = idLop;
    }

    public String getTenLop() {
        return tenLop;
    }

    public void setTenLop(String tenLop) {
        this.tenLop = tenLop;
    }
}
