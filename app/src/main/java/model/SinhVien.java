package model;

import java.io.Serializable;

public class SinhVien implements Serializable {
    private String ten;
    private String msv;
    private String lop;

    private String diemToan;
    private String diemLy;
    private String diemHoa;

    public SinhVien(String ten, String msv, String lop) {
        this.ten = ten;
        this.msv = msv;
        this.lop = lop;
    }

    public SinhVien( String msv, String diemToan, String diemLy, String diemHoa) {
        this.msv = msv;
        this.diemToan = diemToan;
        this.diemLy = diemLy;
        this.diemHoa = diemHoa;
    }

    public String getDiemToan() {
        return diemToan;
    }

    public void setDiemToan(String diemToan) {
        this.diemToan = diemToan;
    }

    public String getDiemLy() {
        return diemLy;
    }

    public void setDiemLy(String diemLy) {
        this.diemLy = diemLy;
    }

    public String getDiemHoa() {
        return diemHoa;
    }

    public void setDiemHoa(String diemHoa) {
        this.diemHoa = diemHoa;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getMsv() {
        return msv;
    }

    public void setMsv(String msv) {
        this.msv = msv;
    }

    public String getLop() {
        return lop;
    }

    public void setLop(String lop) {
        this.lop = lop;
    }
}
